package subdocprovider

import (
	"context"
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"strconv"
	"time"
)

type AliClient struct {
	AccessKey string
	SecretKey string
	Region    string
}

func AlibabaCloudManage() *schema.Resource {
	return &schema.Resource{
		CreateContext: CreateServer,
		ReadContext:   ReadServer,
		DeleteContext: DeleteServer,
		UpdateContext: UpdateServer,
		Schema: map[string]*schema.Schema{
			"bucket": {
				Type:     schema.TypeString,
				Required: true,
			},
			"tags": {
				Type:     schema.TypeMap,
				Optional: true,
			},
		},
	}
}

func AlibabaCloudRead() *schema.Resource {
	return &schema.Resource{
		ReadContext: DataResourceRead,
		Schema: map[string]*schema.Schema{
			"buckets": {
				Type:     schema.TypeList,
				Computed: true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"name": {
							Type:     schema.TypeString,
							Computed: true,
						},
						"creation_date": {
							Type:     schema.TypeString,
							Computed: true,
						},
						"location": {
							Type:     schema.TypeString,
							Computed: true,
						},
					},
				},
			},
		},
	}
}

func Provider() *schema.Provider {
	return &schema.Provider{
		ResourcesMap: map[string]*schema.Resource{
			"alibabaterraform_ossbucketmanage": AlibabaCloudManage(),
		},
		DataSourcesMap: map[string]*schema.Resource{
			"alibabaterraform_ossbucket": AlibabaCloudRead(),
		},
		Schema: map[string]*schema.Schema{
			"access_key": {
				Type:     schema.TypeString,
				Required: true,
			},
			"secret_key": {
				Type:     schema.TypeString,
				Required: true,
			},
			"region": {
				Type:     schema.TypeString,
				Required: true,
			},
		},
		ConfigureContextFunc: providerConfigure,
	}
}

func providerConfigure(ctx context.Context, r *schema.ResourceData) (interface{}, diag.Diagnostics) {
	var diags diag.Diagnostics
	region := r.Get("region").(string)
	access_key := r.Get("access_key").(string)
	secret_key := r.Get("secret_key").(string)

	var cli = AliClient{
		AccessKey: access_key,
		SecretKey: secret_key,
		Region:    region,
	}

	return cli, diags
}

func ServerProvider(ali AliClient, bucket string, tags map[string]interface{}, operate string) (oss.ListBucketsResult, error) {
	endpoint := fmt.Sprintf("https://oss-%s.aliyuncs.com", ali.Region)
	client, err := oss.New(endpoint, ali.AccessKey, ali.SecretKey)
	var bucketinfo oss.ListBucketsResult
	if err != nil {
		return bucketinfo, err
	}

	switch operate {
	case "CreateBucket":
		err = client.CreateBucket(bucket)
		if err != nil {
			return oss.ListBucketsResult{}, err
		}
		var tagging oss.Tagging
		for k, v := range tags {
			tagging.Tags = append(tagging.Tags, oss.Tag{
				Key:   k,
				Value: v.(string),
			})
		}
		err = client.SetBucketTagging(bucket, tagging)
	case "UpdateBucket":
		var tagging oss.Tagging
		for k, v := range tags {
			var tag oss.Tag
			tag.Key = k
			tag.Value = v.(string)
			tagging.Tags = append(tagging.Tags, tag)
		}
		err = client.SetBucketTagging(bucket, tagging)
	case "GetBucket":
		bucketinfo, err = client.ListBuckets()
		if err == nil {
			return bucketinfo, nil
		}
	case "DeleteBucket":
		err = client.DeleteBucket(bucket)
	}
	if err != nil {
		return bucketinfo, err
	}
	return bucketinfo, nil
}

func GetTags(ali AliClient, bucket string) (map[string]interface{}, error) {
	endpoint := fmt.Sprintf("https://oss-%s.aliyuncs.com", ali.Region)
	client, err := oss.New(endpoint, ali.AccessKey, ali.SecretKey)
	if err != nil {
		return nil, err
	}
	re, err := client.GetBucketTagging(bucket)
	if err != nil {
		return nil, err
	}
	var tag = make(map[string]interface{})
	for _, t := range re.Tags {
		tag[t.Key] = t.Value
	}
	return tag, nil
}

func CreateServer(c context.Context, r *schema.ResourceData, i interface{}) diag.Diagnostics {
	ali := i.(AliClient)
	bucket := r.Get("bucket").(string)
	tag := r.Get("tags").(map[string]interface{})
	_, err := ServerProvider(ali, bucket, tag, "CreateBucket")
	if err != nil {
		diag.FromErr(err)
	}
	//var diagd diag.Diagnostics
	r.SetId(ali.Region + ali.AccessKey)
	return ReadServer(c, r, i)
}

func ReadServer(c context.Context, r *schema.ResourceData, i interface{}) diag.Diagnostics {
	ali := i.(AliClient)
	bucket := r.Get("bucket").(string)
	tag, err := GetTags(ali, bucket)
	if err != nil {
		diag.FromErr(err)
	}
	res, err := ServerProvider(ali, bucket, nil, "GetBucket")
	if err != nil {
		diag.FromErr(err)
	}
	var diagd diag.Diagnostics
	var buckets []map[string]interface{}
	for _, v := range res.Buckets {
		var bucket = map[string]interface{}{
			"name":        v.Name,
			"location":    v.Location,
			"create_date": v.CreationDate.Format("2006-02-06 18:06:05"),
		}
		buckets = append(buckets, bucket)
	}
	//b, err := json.Marshal(buckets)
	if err := r.Set("bucket", buckets); err != nil {
		diag.FromErr(err)
	}
	//t,err := json.Marshal(tag)
	if err = r.Set("tags", tag); err != nil {
		diag.FromErr(err)
	}
	//r.Set("tags",tag)
	return diagd
}

func DataResourceRead(c context.Context, r *schema.ResourceData, i interface{}) diag.Diagnostics {
	ali := i.(AliClient)
	//bb := r.Get("bucket").(string)
	//tag := r.Get("tags").(map[string]interface {})
	res, err := ServerProvider(ali, "", nil, "GetBucket")
	if err != nil {
		diag.FromErr(err)
	}
	var diagd diag.Diagnostics
	var buckets []map[string]interface{}
	for _, v := range res.Buckets {
		var bucket = map[string]interface{}{
			"name":          v.Name,
			"location":      v.Location,
			"creation_date": v.CreationDate.Format("2006-01-02 15:04:05"),
		}
		buckets = append(buckets, bucket)
	}
	//b , err:= json.Marshal(buckets)
	if err := r.Set("buckets", buckets); err != nil {
		diag.FromErr(err)
	}
	r.SetId(ali.AccessKey + strconv.FormatInt(time.Now().Unix(), 10))
	return diagd
}

func DeleteServer(c context.Context, r *schema.ResourceData, i interface{}) diag.Diagnostics {
	ali := i.(AliClient)
	bucket := r.Get("bucket").(string)
	//tag := r.Get("tags").(map[string]interface {})
	_, err := ServerProvider(ali, bucket, nil, "DeleteBucket")
	if err != nil {
		diag.FromErr(err)
	}
	var diagd diag.Diagnostics
	r.SetId("")
	return diagd
}

func UpdateServer(c context.Context, r *schema.ResourceData, i interface{}) diag.Diagnostics {
	ali := i.(AliClient)
	bucket := r.Get("bucket").(string)
	tag := r.Get("tags").(map[string]interface{})
	_, err := ServerProvider(ali, bucket, tag, "UpdateBucket")
	if err != nil {
		diag.FromErr(err)
	}
	var diagd diag.Diagnostics
	if err := r.Set("bucket", bucket); err != nil {
		diag.FromErr(err)
	}
	//r.Set("tags",tag)
	return diagd
}
