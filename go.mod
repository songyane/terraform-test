module terraform_test_provider

go 1.15

require (
	github.com/aliyun/alibaba-cloud-sdk-go v1.61.1018 // indirect
	github.com/aliyun/aliyun-oss-go-sdk v2.1.7+incompatible
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.5.0
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba // indirect
	google.golang.org/appengine v1.6.6 // indirect
)
