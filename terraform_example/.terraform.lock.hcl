# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "gitlab.com/myterra/alibabaterraform" {
  version     = "0.3.0"
  constraints = "0.3.0"
  hashes = [
    "h1:JDFPa18J8W8LaXf/d1RATVjPGja9acx93SEzaPUJXb0=",
  ]
}
